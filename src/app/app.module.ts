import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {CheckBoxComponent} from './dynamic-form-builder/atoms/checkbox';
import {ReactiveFormsModule} from '@angular/forms';
import {DynamicFormBuilderModule} from './dynamic-form-builder/dynamic-form-builder.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    DynamicFormBuilderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
